export const DAYS_IN_WEEK = 7;

export const PRESET_TYPES = {
  WEEKS: 'WEEKS',
};

export const PRESET_DEFAULTS = {
  WEEKS: {
    TIMEFRAME_LENGTH: 2,
  },
};
